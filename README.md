Run The code command:

```
$ https://gitlab.com/hasanli/taskify.git
$ docker-compose -f docker-compose.yaml up --build
```

Run the Test command:

```
$ coverage run --omit='*/.venv/*' manage.py test
$ coverage report
$ coverage html
```

PS : This is how the code works

1) Admin user registers (Register)
2) Login to the site (Login)
3) Creates a company (Create Organization)
4) Creates more than one team within the company. (Create Team)
5) Add members to those teams. A warning message is sent to the emails of the added members. (Create Member)

5) Tasks are given to those members. A warning message of the given tasks is sent to the e-mail. (Create Task)