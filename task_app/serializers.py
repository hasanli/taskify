from rest_framework import serializers
from task_app.models import *

class CreateTaskSerializers(serializers.ModelSerializer):
    class Meta:
        model = CreateTaskModel
        fields = '__all__'
