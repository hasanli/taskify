from django.http import HttpResponseRedirect
from django.shortcuts import render,redirect
from task_app.serializers import *
from rest_framework.views import APIView
from user_app.models import OrganizatonsTeamsModel
from rest_framework.response import Response
from rest_framework import status
from task_app.models import *
from rest_framework import generics,permissions


class CreateTaskViews(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CreateTaskSerializers

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response({
                "success_message": "Success",
                "status": status.HTTP_201_CREATED
            })
        else:
            return Response({
                "error_message": serializer.errors,
                "status": status.HTTP_400_BAD_REQUEST
            })


# def verify_task_view(request, token, id):
#     verify = TokenModelTask.objects.filter(
#         token=token,
#         expired=False,
#         user_id=id
#     ).last()
#     if verify:
#         verify.expired = True
#         verify.save()
#         print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++')
#         return HttpResponseRedirect(
#             redirect_to=f"http://0.0.0.0:88/{token}/{id}/")
#     else:
#         print('-------------------------------------------------------')
#         return redirect('error_404_page')
#
#
# def error_404_views(request):
#     return render(request,'404.html')
#
# def success_200_views(request):
#     return render(request, 'success.html')