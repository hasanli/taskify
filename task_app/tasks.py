from __future__ import absolute_import, unicode_literals
from django.core.mail import EmailMultiAlternatives


def send_mail_task(to_mail,link):
    subject, from_email, to = 'Taskify', 'cavidan.hasanli98@gmail.com', to_mail
    text_content = 'Click for Verify account'
    text_content = "Size yeni task verildi. Ugurlar"
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(text_content, 'text/html')
    msg.send()