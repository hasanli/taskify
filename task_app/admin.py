from django.contrib import admin
from task_app.models import CreateTaskModel,TokenModelTask
# Register your models here.

admin.site.register(CreateTaskModel)
admin.site.register(TokenModelTask)