from django.db.models.signals import post_save
from django.dispatch import receiver
from task_app.models import CreateTaskModel, TokenModelTask
from task_app.tasks import send_mail_task
from threading import Thread
from user_app.models import TeamMembers

print('SIGNAL FOR Task VERIFY')


@receiver(post_save, sender=CreateTaskModel)
def create_token(sender, instance, created, **kwargs):

    if created:

        TokenModelTask.objects.create(
            user=instance.member_id.member_id
        )


@receiver(post_save, sender=TokenModelTask)
def user_verify(sender, instance, created, **kwargs):
    if created:
        link = f'http://localhost:8000/verify_task/{instance.token}/{instance.user.id}/'
        background = Thread(target=send_mail_task, args=(instance.user.email, link))
        background.start()