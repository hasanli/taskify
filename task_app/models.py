from django.db import models
from user_app.models import CreateOrganizationModel, TeamMembers, User, OrganizatonsTeamsModel
from user_app.models import token_generator


# Create your models here.

class CreateTaskModel(models.Model):
    organization_id = models.ForeignKey(CreateOrganizationModel, on_delete=models.CASCADE)
    team_id = models.ForeignKey(OrganizatonsTeamsModel, on_delete=models.CASCADE,)
    member_id = models.ForeignKey(TeamMembers, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    deadline = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return f'{self.organization_id} / {self.team_id} / {self.member_id} / {self.title}'


class TokenModelTask(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    expired = models.BooleanField(default=False)
    create_date = models.DateField(auto_now=True)
    token = models.CharField(max_length=55, default=token_generator)

    def __str__(self):
        return f'{self.user},{self.create_date}'
