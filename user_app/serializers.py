from django.contrib import auth
from rest_framework.exceptions import AuthenticationFailed

from user_app.models import User, CreateOrganizationModel, OrganizatonsTeamsModel, TeamMembers
from rest_framework import serializers


# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])

        return user


class CreateOrganizationSerializers(serializers.ModelSerializer):
    # token = serializers.CharField()

    class Meta:
        model = CreateOrganizationModel
        fields = '__all__'


class OrganizatonsTeamsSerializers(serializers.ModelSerializer):
    class Meta:
        model = OrganizatonsTeamsModel
        fields = '__all__'


class TeamMemberSerializers(serializers.ModelSerializer):
    class Meta:
        model = TeamMembers
        fields = ['team_id', 'member_id', 'member_status']
