from rest_framework.views import APIView
from rest_framework import status, permissions
from user_app.models import *
from rest_framework import generics, permissions
from rest_framework.response import Response
from user_app.serializers import RegisterSerializer, \
    CreateOrganizationSerializers, \
    OrganizatonsTeamsSerializers, \
    TeamMemberSerializers
from django.contrib.auth import login
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView
from user_app.signals import *
# from rest_framework.reverse import reverse
from django.http import HttpResponse, HttpResponseRedirect
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


# Register API
class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            password = serializer.validated_data['password']
            if len(password) > 6 and str(password).isalnum():
                serializer.save()
                return Response({
                    "success_message": "Success",
                    "status": status.HTTP_201_CREATED
                })
            else:
                return Response({
                    "error_message": "Password is not matching",
                    "status": status.HTTP_400_BAD_REQUEST
                })

        return Response({
            "error_message": serializer.errors,
            "status": status.HTTP_400_BAD_REQUEST
        })



# Login API
class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = AuthTokenSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)


# Create Organization Api
class CreateOrganizationViews(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CreateOrganizationSerializers

    def post(self, request, format=None):

        serializers = self.serializer_class(data=request.data)

        if serializers.is_valid():
            serializers = serializers.save()
            serializers.organization_admin = request.user
            serializers.save()
            return Response({
                "success_message": "Success",
                "status": status.HTTP_201_CREATED
            })

        return Response({
            "error_message": serializers.errors,
            "status": status.HTTP_400_BAD_REQUEST
        })


# Create Team Api
class CreateTeamViews(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = OrganizatonsTeamsSerializers

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            serializer = serializer.save()
            serializer.team_lead = request.user
            serializer.save()
            return Response({
                "success_message": "Success",
                "status": status.HTTP_201_CREATED
            })

        return Response({
            "error_message": serializer.errors,
            "status": status.HTTP_400_BAD_REQUEST
        })


class CreateTeamMemberViews(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = TeamMemberSerializers
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                "success_message": "Success",
                "status": status.HTTP_201_CREATED
            })

        return Response({
            "error_message": serializer.errors,
            "status": status.HTTP_400_BAD_REQUEST
        })


def verify_view(request, token, id):
    verify = TokenModelTeam.objects.filter(
        token=token,
        expired=False,
        user_id=id
    ).last()
    if verify:
        a = TeamMembers.objects.filter(member_id=id).last()
        a.is_active = True
        a.save()
        verify.expired = True
        verify.save()
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        return HttpResponseRedirect(
            redirect_to="https://mail.google.com/mail/u/1/#inbox/FMfcgxwLsdHTqRsBGwzbGBcNSXwrRxfl")
    else:
        print('-------------------------------------------------------')
        return HttpResponseRedirect(redirect_to="https://mail.google.com/mail/")
