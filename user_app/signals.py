from django.db.models.signals import post_save
from django.dispatch import receiver
from user_app.models import TeamMembers, TokenModelTeam
from user_app.tasks import send_mail_task
from threading import Thread

print('SIGNAL FOR Team VERIFY')


@receiver(post_save, sender=TeamMembers)
def create_token(sender, instance, created, **kwargs):

    if created:
        print("AAAAAAAAAAAAAAAAAAAAAAAAAAAA",instance.member_id)
        TokenModelTeam.objects.create(
            user=instance.member_id
        )


@receiver(post_save, sender=TokenModelTeam)
def user_verify(sender, instance, created, **kwargs):
    if created:
        link = f'http://localhost:8000/verify_team/{instance.token}/{instance.user.id}/'
        background = Thread(target=send_mail_task, args=(instance.user.email, link))
        background.start()