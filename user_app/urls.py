from django.urls import path
from user_app.views import *
from knox import views as knox_views

urlpatterns = [
    path('api/v1/create_organization/', CreateOrganizationViews.as_view(), name='create_organization'), # sirket yaradir
    path('api/v1/register/', RegisterAPI.as_view(), name='register'), # qeydiyyat edir
    path('api/v1/login/', LoginAPI.as_view(), name='login'), # login olur
    path('api/v1/logout/', knox_views.LogoutView.as_view(), name='logout'),
    path('api/v1/create_team/', CreateTeamViews.as_view(), name='create_team'), # her sirkete vezifelerine uygun komanda yaradir
    path('api/v1/create_member/',CreateTeamMemberViews.as_view(),name='create_member'), # her teamin ozune isci elave edir
    path('verify_team/<str:token>/<int:id>/', verify_view, name='verify_view'), # maile geden link budur
]