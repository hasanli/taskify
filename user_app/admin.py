from django.contrib import admin
from user_app.models import *
from django.contrib.auth.models import Group

admin.site.unregister(Group)
admin.site.register(CreateOrganizationModel)
admin.site.register(OrganizatonsTeamsModel)
admin.site.register(TeamMembers)
admin.site.register(TokenModelTeam)

# Register your models here.
