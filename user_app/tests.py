import json

from user_app.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from faker import Faker


class RegistrationTestCase(APITestCase):

    def test_registration(self):
        self.fake = Faker()
        data = {"username": self.fake.email().split('@')[0],
                "email": self.fake.email(),
                "password": self.fake.email()}
        response = self.client.post("/users/api/v1/register/", data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
