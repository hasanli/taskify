import uuid as uuid
from django.db import models
from django.contrib.auth.models import User



# Create your models here.

def token_generator():
    id = uuid.uuid4()
    return str(id)

class CreateOrganizationModel(models.Model):
    organization_admin = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    # uuid = models.UUIDField(auto_created=uuid.uuid4, default=uuid.uuid4, editable=False, blank=True)
    organization_name = models.CharField(max_length=150, null=True)
    created_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    phone_number = models.CharField(max_length=13, null=True, blank=True, unique=True)
    address = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return f'{self.organization_name}'


class OrganizatonsTeamsModel(models.Model):
    organization = models.ForeignKey(CreateOrganizationModel, on_delete=models.DO_NOTHING)
    team_lead = models.ForeignKey(User, on_delete=models.CASCADE,)
    team_name = models.CharField(max_length=255)
    start_time = models.DateTimeField(auto_now=True,null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return f'{self.team_name}'


class TeamMembers(models.Model):
    team_id = models.ForeignKey(OrganizatonsTeamsModel, related_name='files', on_delete=models.CASCADE)
    member_id = models.ForeignKey(User, related_name='member',on_delete=models.CASCADE)
    member_status = models.CharField(max_length=244)
    is_active = models.BooleanField('active',
                                    default=False,
                                    help_text=
                                    'Designates whether this user should be treated as active. '
                                    'Unselect this instead of deleting accounts.'
                                    ),

    number = models.CharField(max_length=3, null=True, blank=True)

    class Meta:
        ordering = ["-id"]

    def __str__(self):
        return f'{self.member_id}'


class TokenModelTeam(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    expired = models.BooleanField(default=False)
    create_date = models.DateField(auto_now=True)
    token = models.CharField(max_length=55, default=token_generator)

    def __str__(self):
        return f'{self.user},{self.create_date}'