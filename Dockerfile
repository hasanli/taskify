FROM python:3.8


WORKDIR /app

COPY requirements.txt ./

RUN pip install -U pip && \
    pip install -r requirements.txt

COPY . .

EXPOSE 80
ENTRYPOINT ["/bin/bash", "start.sh"]
